<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
            // Ce n'est pas très beau comme affichage...
             // Affichez le contenu complet du fichier notes.txt en faisant respecter les sauts de lignes
             // Faites le avec une boucle et sans boucle
             // https://www.php.net/manual/fr/function.nl2br.php
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
        	<?php
                
                $i = file_get_contents("notes.txt");
                echo nl2br ($i);

                $j = file("notes.txt");

                echo '<br><br><br><br><br>';

                foreach($j as $k){
                    echo $k . '<br>';
                }
                
            ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>