<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // fgets ou fgetc ???
             // Affichez les 3 premières ligne du fichier notes.txt
             // Ligne 1 : Le 30/01/2020, nous allons apprendre que
             // ...   
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
            	<?php
                   $source = fopen("notes.txt", 'r');
                   echo fgets($source).'<br>';
                   echo fgets($source).'<br>';
                   echo fgets($source).'<br>'; 
                   echo fgets($source).'<br>'; 
                   echo fgets($source).'<br>'; 
                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>