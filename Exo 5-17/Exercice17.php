<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
    <style>
    body {
        width           : 100%;
        background-color: rgb(225, 213, 228);
        text-align      : center;
        font-size       : 25px;
    }

    .container {
        width          : 90%;
        display        : flex;
        justify-content: space-around;
        margin         : 0 auto;
        flex-direction: row;
    }

    .container .bloc {
        padding: 0 20px;
    }

    .bloc {
        background-color: rgb(218, 189, 224);
        box-shadow      : 0 0 7px grey;
        text-align      : center;
        justify-items   : center;
        padding         : 10px;
        border-radius   : 10px;
    }

    .taille {
        width    : 75%;
        margin   : 0 auto;
        font-size: 30px;
    }

    p {
        font-family: Arial, Helvetica, sans-serif;

    }

    @media (max-width:700px){

        .container{
            flex-direction: column;
        }

        .container .bloc {
            padding: 10px;
            margin: 10px 0;
        }

        .br {
            display:none;
        }

        .taille {
            width    : 85%;
            font-size: 25px;
        }
    }
    </style>
</head>
    <body>
            
        <?php
    
             // Créer un script afin de récupérer toutes les infos de l'Ordonnance.txt qu'a rempli le Dr Strauss
             // Récupérer les coordonnées du patient, celle du docteur, son hopital, la date, les médicaments et la personne qui à fait l'ordonnance
             // Affichez comme y faut les informations dans une jolie page html bien stylisée
             // Lorsque que vous pensez avoir fini, venez me voir avec votre pc que je test votre code
            
        ?>
        
        <!-- écrire le code après ce commentaire -->

        <?php
                    $source = fopen("Ordonnance.txt", 'r');

                    for($i =0; $i<=2; $i++){
                        $vide = nl2br(fgets($source));
                    }
                    $patient= fgets($source);
                    $adressPatient= fgets($source);
                    $villePatient= fgets($source);
                    $prescripteur= fgets($source);
                    $medecin= fgets($source);
                    $hopital= fgets($source); 
                    $date= fgets($source, 14);
                    $date= str_replace("Le", "", $date);
                    fgets($source);
                    $vide= fgets($source);
                    $nbmedoc=fgets($source);
                    $nbmedoc= str_replace("Nombres de médicaments : ", "",$nbmedoc);
                    for($j =0; $j<=2; $j++){
                        (fgets($source));
                    } 
                    $brut= "";
                    for($j =0; $j<$nbmedoc; $j++){
                        $brut.=fgets($source).'<br>';
                    } 
                    $oct= str_replace("-", "",$brut);


                ?>

            <h1>Ordonnance</h1>

        <div class="container">
            

            <div class="bloc">
                <p>Information patient :</p>
                <?php
                echo "<p>$patient</p>".
                "<p>$adressPatient</p>".
                "<p>$villePatient</p>";
                ?>
            </div>

            <div class="bloc">
                <p>Information médecin :</p>
                <?php
                echo "<p><br></p>".
                "<p>$medecin</p>".
                "<p>$hopital</p>";
                ?>
            </div>
        </div>

            <p><?php echo "Date : $date" ?></p>

        <div class="taille bloc">
            <?php
            echo "<p>$oct</p>";
            ?>
        </div>
        
        <br>
        <div class="container">
            <p><?php 
            $faitepar= fgets($source);
            $faitepar= fgets($source);

            echo $faitepar; ?></p>
        </div>            
        

            
            
            
            
        







            	
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>