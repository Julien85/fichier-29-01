<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Affichez le contenu du fichier first.txt avec la fonction file();
             // Affichez le sans print_r ou var_dump
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
        	<?php

              $x = file("first.txt");

              foreach($x as $cle){
                  echo $cle;
              }
    
            ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>