<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Affichez le contenu du fichier first.txt avec la fonction file_get_contents();
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
        	<?php
                
              $contenu = file_get_contents("first.txt");

              echo $contenu;
                
            ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>