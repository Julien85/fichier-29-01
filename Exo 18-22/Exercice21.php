<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Affichez ce que contient chaques lignes, et comptez le nombre de caratères dans chaques lignes
             // Enlevez le retour chariot, et affichez un s à caractére s'il y en a plus d'un 1
             // Exemple : La ligne " 1 "  contient 1 caractère
             // Exemple : La ligne " 10 " contient 2 caractères
             // PS : le fichier peux contenir entre 99 et 101 chiffres
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
            	<?php
                   $source= fopen('Chiffres.txt', 'r');
                    
                   while(!feof($source)){
                       $ligne= fgets($source);
                       $size= strlen($ligne);

                       if(feof($source)){
                            echo "La ligne " .$ligne . " contient " . $size . " caractères" .'<br>';
                        }elseif($size == 2){
                            echo "La ligne " .$ligne . " contient " . ($size-1) . " caractère" .'<br>';
                       }else{
                            echo "La ligne " .$ligne . " contient " . ($size-1) . " caractères" .'<br>';
                       }                       
                   }
                ?>
            
        <!-- écrire le code avant ce commentaire -->
    </body>
</html>