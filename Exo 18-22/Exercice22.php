<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Ecrire le code permetttant de faire un compteur de pages.
             // Vous pouvez prendre le fichier notes.txt pour écrire dedans
             // Votre code ouvre le fichier, place le curseur au début, écrit le chiffre du compteur,
             // ferme le fichier et affiche le nouveau résultat.
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
            	<?php
                    
                  $source= fopen('notes.txt', 'r+b');
                  $size= filesize('notes.txt');
                  $contenu= fread($source, $size); 
                  $i= $contenu+1;
                  fseek($source, 0);
                  fwrite($source, $i);
                  fclose($source);
                   echo $i;

                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>